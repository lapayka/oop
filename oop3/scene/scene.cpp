#include "scene.h"

void Scene::accept(Visitor &visitor)
{
    for (const auto &elem : *objects)
        elem->accept(visitor);
}

void Scene::conversion(const Point &transfer, const ScaleCoef &scale, const Angle &rotate)
{
    for (auto &object : *objects)
        object->conversion(transfer, scale, rotate);
}

void Scene::set_camera(int index)
{
    IteratorObject it = objects->begin() + index;

    set_camera(it);
}

void Scene::set_camera(const IteratorObject &it)
{
    if (it != objects->end())
    {
        camera = (*it);
    }
}

shared_ptr<Object> Scene::get_camera()
{
    return camera.lock();
}

