#include "scenemanager.h"

SceneManager::SceneManager()
{

}


void SceneManagerCreator::create_manager()
{
    static shared_ptr<SceneManager>_manager(new SceneManager);
    manager = _manager;
}

shared_ptr<SceneManager> SceneManagerCreator::get_manager()
{
    if (manager == nullptr)
        create_manager();

    return manager;
}
