#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "basemanager.h"
#include "scene/scene.h"

class SceneManager : public BaseManager
{
public:
    SceneManager();

    shared_ptr<Scene> get_scene() const {return scene; };
    void set_scene(shared_ptr<Scene> &_scene) {scene = _scene; };

    shared_ptr<Object> get_camera() const {return scene->get_camera(); }
    void set_camera(int it) {scene->set_camera(it); }
private:
    shared_ptr<Scene> scene;
};

class SceneManagerCreator
{
public:
    std::shared_ptr<SceneManager> get_manager();
private:
    void create_manager();

    std::shared_ptr<SceneManager> manager;
};

#endif // SCENEMANAGER_H
