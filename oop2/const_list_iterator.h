#ifndef CONST_LIST_ITERATOR_H
#define CONST_LIST_ITERATOR_H

#include "node.h"
#include "exceptions.h"
#include <chrono>
#include <memory>

template <typename Type>
class list;

template <typename Type>
class const_list_iterator : public std::iterator<std::forward_iterator_tag, node<Type>>
{
    friend class list<Type>;
private:
    std::weak_ptr<node<Type>> cur;

    std::shared_ptr<node<Type>> &get(){return cur.lock(); };
    const_list_iterator(const std::shared_ptr<node<Type>> &pnode) {cur = std::weak_ptr<node<Type>>(pnode); };
public:
    const_list_iterator(const const_list_iterator &it) = default;

    bool operator!=(const_list_iterator const &other) const ;
    bool operator==(const_list_iterator const &other) const ;

    const Type &operator*() const {return cur.lock()->get();  };
    const Type *operator->() const {return cur.lock()->get_pt(); };

    virtual bool is_invalid() {return cur.expired(); };

    operator bool (){ return is_invalid() ;};

    const_list_iterator<Type>& operator++() ;
    const_list_iterator<Type> operator++(int);

    const_list_iterator<Type> operator+(int step) const;
};

#include "const_list_iterator.hpp"


#endif // CONST_LIST_ITERATOR_H
