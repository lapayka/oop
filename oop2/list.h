#ifndef LIST_H
#define LIST_H

#include <iostream>
#include <memory>
#include "node.h"
#include "exceptions.h"
#include "base.h"
#include "list_iterator.h"
#include "const_list_iterator.h"
#include <chrono>

template <typename Type>
class list : public container
{
public:
    list() = default;

    explicit list(const list<Type> &list);
    list(list<Type> &&list) noexcept;
    list(Type *const array, int size);
    list(std::initializer_list<Type> values);

    void push_front(const Type &data);
    void push_front(const list<Type> &push_list);

    void insert(list_iterator<Type>, const Type &data);
    void insert(list_iterator<Type>, const list<Type> &insert_list);
    template<typename Iterator_type>
    void insert(list_iterator<Type>, Iterator_type begin, Iterator_type end);

    void push_back(const Type &data);
    void push_back(const list<Type> &data);

    Type pop_front();
    Type pop_back();

    void remove(const list_iterator<Type> &);
    void remove(const list_iterator<Type> &begin, int count);
    void remove(const list_iterator<Type> &begin, const list_iterator<Type> &end);

    void reverse();

    list_iterator<Type> begin() {return list_iterator(head);}
    list_iterator<Type> end() {return ++list_iterator(tail);}

    const_list_iterator<Type> begin() const {return cbegin(); };
    const_list_iterator<Type> end() const {return cend(); };

    const_list_iterator<Type> cbegin() const {return const_list_iterator(head); };
    const_list_iterator<Type> cend() const {return ++const_list_iterator(tail); };

    list<Type> &merge(const list<Type> &list);

    bool equal(const list<Type> &list) const;
    bool not_equal(const list<Type> &list) const;

    list<Type> &operator += (const list<Type> &list);
    list<Type> &operator += (const Type &data);

    list<Type> operator + (const list<Type> &list) const;
    list<Type> operator + (const Type &data) const;

    bool operator == (const list<Type> &list) const;
    bool operator != (const list<Type> &list) const;

    list<Type> &operator = (const list<Type> &list);
    list<Type> &operator = (list<Type> &&list) noexcept;
    list<Type> &operator = (std::initializer_list<Type> values);

    explicit operator bool() const {return is_empty(); };

    virtual bool is_empty()
    {
        return size == 0;
    };

    virtual void clear()
    {
        while (size--)
            pop_front();
    };

    virtual ~list() = default;

private:
    std::shared_ptr<node<Type>> head;
    std::shared_ptr<node<Type>> tail;
    int size;

    std::shared_ptr<node<Type>> create_node(const Type &);

};

#include "list.hpp"

#endif // LIST_H
