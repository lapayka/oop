#include <iostream>
#include "list.h"
#include "assert.h"

using namespace std;

void add_test()
{
    list<int> obj1 = {1, 2 ,4 , 2};
    list<int> obj2 = {3, 5};

    assert(list<int>({1, 2, 4, 2, 3, 5}) == (obj1 += obj2));
    assert(list<int>({1, 2, 4, 2, 3, 5}) == obj1);

    assert(list<int>({1, 2, 4, 2, 3, 5, 7}) == (obj1 + 7));
    assert(list<int>({1, 2, 4, 2, 3, 5, 3, 5}) == (obj1 + obj2));
}

void remove_test()
{
    list<int> obj1 = {1, 2 ,4 , 2};
    obj1.remove(obj1.begin() + 2);

    list<int> obj2 = {1, 2 ,4 , 2};
    obj2.remove(obj2.begin(), obj2.begin() + 2);

    list<int> obj3 = {1, 2, 4, 2};
    obj3.remove(obj3.begin() + 1, 3);

    assert(obj1 == list<int>({1, 2, 2}));
    assert(obj2 == list<int>({2}));
    assert(obj3 == list<int>({1}));
}

void reverse_test()
{
    list<int> obj = {1, 2 ,4 , 2};

    obj.reverse();


    assert(obj == list<int>({2, 4, 2, 1}));
}

void insert_list_test()
{
    list<int> obj = {1, 2, 3, 4};

    obj.insert(obj.begin() + 2, list<int>({10, 9}));

    assert(obj == list<int>({1, 2, 3, 10, 9, 4}));

    list<int> obj2 = list<int>({30, 40, 50});
    obj2.insert(obj2.begin(), obj.begin() + 2, obj.begin() + 6);

    //for (auto elem : obj2)
    //    cout << elem << " ";

    assert(obj2 == list<int>({30, 3, 10 , 9, 4, 40, 50}));
}

void insert_test()
{
    list<int> obj = {1, 2, 3, 4};
    for (list_iterator it = obj.begin(); it != obj.end(); ++it)
    {
        obj.insert(it, 3);
        ++it;
        if (it == obj.end())
            break;
    }

    assert(obj == list<int>({1, 3, 2, 3, 3, 3, 4 ,3}));

    obj.insert(obj.begin() + 1, list<int>{4, 5, 6});
    assert(obj == list<int>({1, 3, 4, 5, 6, 2, 3, 3, 3, 4 ,3}));
}

void pop_front_test()
{
    list<int> obj = {1, 2, 3, 4};

    assert(obj.pop_front() == 1);
    assert(obj.pop_front() == 2);
    assert(obj.pop_front() == 3);
    assert(obj.pop_front() == 4);
}

void pop_back_test()
{
    list<int> obj = {1, 2, 3, 4};

    assert(obj.pop_back() == 4);
    assert(obj.pop_back() == 3);
    assert(obj.pop_back() == 2);
    assert(obj.pop_back() == 1);
}

void push_front_test()
{
    list<int> obj = {};

    obj.push_front(1);

    assert(obj == list<int>({1}));

    obj.push_front(list<int>({4, 3, 2}));

    assert(obj == list<int>({4, 3, 2, 1}));
}

void push_back_test()
{
    list<int> obj = {};

    obj.push_back(1);

    assert(obj == list<int>({1}));

    obj.push_back(list<int>({4, 3, 2}));

    assert(obj == list<int>({1, 4, 3, 2}));
}

void iterator_test()
{
    list<int> obj = {1, 2, 3, 4 ,5};

    for (auto &elem :obj)
    {
        elem += 1;
    }

    assert(obj == list<int>({2,3,4,5,6}));

    for (auto elem : obj)
        elem += 1;
    assert(obj == list<int>({2,3,4,5,6}));
}

void equal_test()
{
    list<int> obj1 = {1, 2, 3, 4, 5};
    list<int> obj2 = {1, 2, 3, 4, 5};

    assert(obj1 == obj2);
    obj2.push_back(3);

    assert(!(obj1 == obj2));
}

void egor_test()
{
    list<int> obj1 = {1, 2, 3, 4, 5};
    obj1 += {3,7};

    assert(obj1 == list<int>({1, 2, 3, 4, 5, 3,7}));
}

int main()
{
    egor_test();
    equal_test();
    iterator_test();
    push_back_test();
    push_front_test();
    pop_back_test();
    pop_front_test();
    insert_test();
    insert_list_test();
    reverse_test();
    remove_test();
    add_test();
    return 0;
}
