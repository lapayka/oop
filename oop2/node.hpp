#ifndef NODE_HPP
#define NODE_HPP

template <typename Type>
node<Type>::node(const Type &data)
{
    this->data = data;
    next = nullptr;
}

template <typename Type>
void node<Type>::set(const Type &data)
{
    this->data = data;
}

template <typename Type>
void node<Type>::set_next(const node &pnode)
{
    std::shared_ptr<node> node_pt(pnode);
    this->next = node_pt;
}

template <typename Type>
void node<Type>::set_next(const std::shared_ptr<node<Type>> &node)
{
    this->next = node;
}

template <typename Type>
Type &node<Type>::get()
{
    return this->data;
}

template <typename Type>
std::shared_ptr<node<Type>>& node<Type>::get_next()
{
    return this->next;
}

#endif // NODE_HPP
