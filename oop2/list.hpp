#ifndef LIST_HPP
#define LIST_HPP
#include <iostream>

template <typename Type>
class list;

template<typename Type>
list<Type>::list(const list<Type> &copy_list)
    : size(0),
      head(nullptr),
      tail(nullptr)
{
    push_back(copy_list);
}

template<typename Type>
list<Type>::list(list<Type> &&list) noexcept
    : head(list.head),
      tail(list.tail),
      size(list.size)
{
}

template<typename Type>
list<Type>::list(Type *const array, int size)
    : size(0),
      head(nullptr),
      tail(nullptr)
{
    if (!array || size <= 0)
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw args_exception(ctime(&timenow), __FILE__, typeid(list).name(), __FUNCTION__);
    }
    for (int i = 0; i < size; i++)
    {
        this->push_back(*array++);
    }
}

template<typename Type>
list<Type>::list(std::initializer_list<Type> values)
    : size(0),
      head(nullptr),
      tail(nullptr)
{
    for (auto elem : values)
    {
        this->push_back(elem);
    }
}

template<typename Type>
void list<Type>::push_front(const Type &data)
{
    std::shared_ptr<node<Type>> tmp = create_node(data);

    if (head == nullptr)
    {
        tail = head = tmp;
    }
    else
    {
        tmp->set_next(head);
        head = tmp;
    }

    size++;
}


template<typename Type>
void list<Type>::push_front(const list<Type> &push_list)
{
    list<int> buf;
    for (auto elem : push_list)
    {
        buf.push_back(elem);
    }

    buf.tail->set_next(this->head);
    this->head = buf.head;
}

template<typename Type>
void list<Type>::insert(list_iterator<Type> pos, const Type &data)
{
    std::shared_ptr<node<Type>> tmp = create_node(data);

    std::shared_ptr<node<Type>> begin = pos.get();
    tmp->set_next(begin->get_next());
    begin->set_next(tmp);

    if ((pos + 1) == this->end())
        tail = tmp;

    size++;
}

template<typename Type>
void list<Type>::insert(list_iterator<Type> pos, const list<Type> &insert_list)
{
    for (auto elem : insert_list)
    {
        insert(pos, elem);
        ++pos;
    }
}

template<typename Type>
template<typename Iterator_type>
void list<Type>::insert(list_iterator<Type> pos, Iterator_type begin, Iterator_type end)
{
    if (!end.is_invalid())
        ++end;

    for (Iterator_type iter = begin; iter != end; ++iter)
    {
        insert(pos, *iter);
        ++pos;
    }
}

template<typename Type>
void list<Type>::push_back(const Type &data)
{
    std::shared_ptr<node<Type>> tmp = create_node(data);

    if (head == nullptr)
    {
        tail = head = tmp;
    }
    else
    {
        tail->set_next(tmp);
        tail = tmp;
    }
    size++;
}


template<typename Type>
void list<Type>::push_back(const list<Type> &push_list)
{
    for (auto elem : push_list)
    {
        push_back(elem);
    }
}

template<typename Type>
Type list<Type>::pop_front()
{
    if (head == nullptr || tail == nullptr)
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw empty_list_exception(ctime(&timenow), __FILE__, typeid(list).name(), __FUNCTION__);
    }

    Type value = (head->get());

    if (head == tail)
        head = tail = nullptr;
    else
        head = head->get_next();

    size--;
    return value;
}

template<typename Type>
Type list<Type>::pop_back()
{
    if (size == 0)
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw empty_list_exception(ctime(&timenow), __FILE__, typeid(list).name(), __FUNCTION__);
    }

    Type value = tail->get();
    if (size == 1)
        head = tail = nullptr;
    else
    {
        std::shared_ptr<node<Type>> cur_node(head);
        for (; cur_node->get_next() != tail; cur_node = cur_node->get_next())
            ;

        tail = cur_node;
    }
    size--;
    return value;
}

template <typename Type>
void list<Type>::remove(const list_iterator<Type> &iterator)
{
    list_iterator<Type> prev = this->begin();

    if (prev == iterator)
        pop_front();
    else
    {
        for (; prev + 1 != iterator; ++prev)
            ;

        std::shared_ptr<node<Type>> to_remove = iterator.get();
        std::shared_ptr<node<Type>> elem = prev.get();

        elem->set_next(to_remove->get_next());
    }
    size--;
}

template <typename Type>
void list<Type>::remove(const list_iterator<Type> &begin, const list_iterator<Type> &end)
{
    if (begin.is_invalid())
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw args_exception(ctime(&timenow), __FILE__, typeid(list).name(), __FUNCTION__);
    }
    list_iterator<Type> prev = this->begin();

    if (prev == begin)
        if (end.is_invalid())
            head = tail = nullptr;
        else
            head = end.get()->get_next();
    else
    {
        for (; prev + 1 != begin; ++prev)
            ;
        std::shared_ptr<node<Type>> elem = prev.get();
        if (end.is_invalid())
            elem->set_next(nullptr);
        else
        {
            std::shared_ptr<node<Type>> to_remove = end.get();
            elem->set_next(to_remove->get_next());
        }
    }
}

template <typename Type>
void list<Type>::remove(const list_iterator<Type> &begin, int count)
{
    remove(begin, begin + (count - 1));
}

template  <typename Type>
void list<Type>::reverse()
{
    std::shared_ptr<node<Type>> next, cur = head, prev = nullptr, tail_tmp = head;

    while (cur != nullptr)
    {
        next = cur->get_next();
        cur->set_next(prev);

        prev = cur;
        cur = next;
    }
    head = prev;
    tail = tail_tmp;
}

template  <typename Type>
list<Type> &list<Type>::merge(const list<Type> &merge_list)
{
    push_back(merge_list);

    return *this;
}

template  <typename Type>
list<Type> &list<Type>::operator+=(const list<Type> &add_list)
{
    push_back(add_list);

    return *this;
}

template  <typename Type>
list<Type> &list<Type>::operator+=(const Type &data)
{
    push_back(data);

    return *this;
}

template  <typename Type>
list<Type> list<Type>::operator+(const Type &data) const
{
    list<Type> list_copy(*this);

    list_copy.push_back(data);
    return list_copy;
}

template  <typename Type>
list<Type> list<Type>::operator+(const list<Type> &add_list) const
{
    list<Type> list_copy(*this);

    list_copy.push_back(add_list);
    return list_copy;
}

template  <typename Type>
bool list<Type>::operator==(const list<Type> &list) const
{
    bool flag = true;

    std::shared_ptr<node<Type>> a(head), b(list.head);
    for (; flag && a != nullptr && b != nullptr; a = a->get_next(), b = b->get_next())
    {
        flag = a->get() == b->get();
    }

    return flag && a == nullptr && b == nullptr;
}

template  <typename Type>
bool list<Type>::operator!=(const list<Type> &list) const
{
    return !(list == *this);
}

template  <typename Type>
list<Type> &list<Type>::operator=(const list<Type> &list)
{
    head = tail = nullptr;
    size = 0;

    push_back(list);

    return *this;
}

template  <typename Type>
list<Type> &list<Type>::operator=(std::initializer_list<Type> init_list)
{
    for (auto elem : init_list)
        push_back(elem);

    return (*this);
}

template  <typename Type>
list<Type> &list<Type>::operator=(list<Type> &&list) noexcept
{
    head = list.head;
    tail = list.tail;
    size = list.size;

    return *this;
}

template  <typename Type>
std::shared_ptr<node<Type>> list<Type>::create_node(const Type &data)
{
    try
    {
        return std::shared_ptr<node<Type>>(new node(data));
    }
    catch (std::bad_alloc)
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw memory_exception(ctime(&timenow), __FILE__, typeid(list).name(), __FUNCTION__);
    }
}



#endif // LIST_HPP
