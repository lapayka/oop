#ifndef BASE_H
#define BASE_H

class container
{
protected:
    int size;

public:
    virtual bool is_empty() = 0;
    virtual void clear() = 0;
    virtual ~container() = default;

};

#endif // BASE_H
