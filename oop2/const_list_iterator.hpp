#ifndef CONST_LIST_ITERATOR_HPP
#define CONST_LIST_ITERATOR_HPP

template <typename Type>
bool const_list_iterator<Type>::operator!=(const_list_iterator const &other) const
{
    return cur.lock() != other.cur.lock();
}

template <typename Type>
bool const_list_iterator<Type>::operator==(const_list_iterator const &other) const
{
    return cur.lock() == other.cur.lock();
}

template <typename Type>
const_list_iterator<Type>& const_list_iterator<Type>::operator++()
{
    if (is_invalid())
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw iterator_exception(ctime(&timenow), __FILE__, typeid(const_list_iterator).name(), __FUNCTION__);
    }
    cur = std::weak_ptr(cur.lock()->get_next());
    return *this;
}

template <typename Type>
const_list_iterator<Type> const_list_iterator<Type>::operator++(int)
{
    if (is_invalid())
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw iterator_exception(ctime(&timenow), __FILE__, typeid(const_list_iterator).name(), __FUNCTION__);
    }
    const_list_iterator copy(*this);
    cur = std::weak_ptr(cur.lock()->get_next());
    return copy ;
}

template <typename Type>
const_list_iterator<Type> const_list_iterator<Type>::operator+(int step) const
{
    if (is_invalid())
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw iterator_exception(ctime(&timenow), __FILE__, typeid(const_list_iterator).name(), __FUNCTION__);
    }
    const_list_iterator tmp(cur);
    while (step--)
    {
        ++tmp;
    }
    return tmp;
}

#endif // CONST_LIST_ITERATOR_HPP
