#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <memory>

template <typename Type>
class node
{
    std::shared_ptr<node> next;
    Type data;

public:
    node() = default;
    node(const Type &data);

    void set(const Type &data);
    void set_next(const node &pnode);
    void set_next(const std::shared_ptr<node<Type>> &node);

    Type &get();
    std::shared_ptr<node> &get_next();
    Type *get_pt() {return &data; };
};

#include "node.hpp"

#endif // NODE_H
