#ifndef LIST_ITERATOR_HPP
#define LIST_ITERATOR_HPP

template <typename Type>
bool list_iterator<Type>::operator!=(list_iterator const &other) const
{
    return cur.lock() != other.cur.lock();
}

template <typename Type>
bool list_iterator<Type>::operator==(list_iterator const &other) const
{
    return cur.lock() == other.cur.lock();
}

template <typename Type>
list_iterator<Type>& list_iterator<Type>::operator++()
{
    if (is_invalid())
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw iterator_exception(ctime(&timenow), __FILE__, typeid(list_iterator).name(), __FUNCTION__);
    }
    cur = std::weak_ptr(cur.lock()->get_next());
    return *this;
}

template <typename Type>
list_iterator<Type> list_iterator<Type>::operator++(int)
{
    if (is_invalid())
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw iterator_exception(ctime(&timenow), __FILE__, typeid(list_iterator).name(), __FUNCTION__);
    }
    list_iterator copy(*this);
    cur = std::weak_ptr(cur.lock()->get_next());
    return copy ;
}

template <typename Type>
list_iterator<Type> list_iterator<Type>::operator+(int step) const
{
    if (is_invalid())
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw iterator_exception(ctime(&timenow), __FILE__, typeid(list_iterator).name(), __FUNCTION__);
    }

    list_iterator tmp(*this);
    while (step--)
    {
        if (tmp.is_invalid())
        {
            auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            throw args_exception(ctime(&timenow), __FILE__, typeid(list_iterator).name(), __FUNCTION__);
        }
        ++tmp;
    }
    return tmp;
}

template <typename Type>
list_iterator<Type> &list_iterator<Type>::operator+=(int step)
{
    if (is_invalid())
    {
        auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        throw iterator_exception(ctime(&timenow), __FILE__, typeid(list_iterator).name(), __FUNCTION__);
    }

    while (step--)
    {
        if (this->is_invalid())
        {
            auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            throw args_exception(ctime(&timenow), __FILE__, typeid(list_iterator).name(), __FUNCTION__);
        }
        ++(*this);
    }
    return (*this);
}



#endif // LIST_ITERATOR_HPP
