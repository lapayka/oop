#ifndef LIST_ITERATOR_H
#define LIST_ITERATOR_H

#include "node.h"
#include "exceptions.h"
#include <chrono>
#include <memory>

template <typename Type>
class list;

template <typename Type>
class list_iterator : public std::iterator<std::forward_iterator_tag, node<Type>>
{
    friend class list<Type>;
private:
    std::weak_ptr<node<Type>> cur;

    std::shared_ptr<node<Type>> get() const {return cur.lock(); };
    list_iterator(const std::shared_ptr<node<Type>> &pnode) {cur = std::weak_ptr<node<Type>>(pnode); };
public:
    list_iterator(const list_iterator &it) = default;

    bool operator!=(list_iterator const &other) const ;
    bool operator==(list_iterator const &other) const ;

    Type &operator*() {return cur.lock()->get(); };
    const Type &operator*() const {return cur.lock()->get();  };

    Type *operator->() {return cur.lock()->get_pt(); };
    const Type *operator->() const {return cur.lock()->get_pt(); };

    virtual bool is_invalid() const {return cur.expired(); };

    operator bool () const {return is_invalid() ;};

    list_iterator<Type>& operator++() ;
    list_iterator<Type> operator++(int);

    list_iterator<Type> operator+(int step) const;
    list_iterator<Type> &operator+=(int step);
};

#include "list_iterator.hpp"

#endif // LIST_ITERATOR_H
