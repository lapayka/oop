TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp

HEADERS += \
    base.h \
    const_list_iterator.h \
    const_list_iterator.hpp \
    exceptions.h \
    list.h \
    list.hpp \
    list_iterator.h \
    list_iterator.hpp \
    node.h \
    node.hpp
