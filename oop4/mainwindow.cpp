#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , controller()
{
    ui->setupUi(this);

    QObject::connect(this, SIGNAL(set_floor(int)), &controller, SLOT(get_floor(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    emit set_floor(1);
}

void MainWindow::on_pushButton_2_clicked()
{
    emit set_floor(2);
}

void MainWindow::on_pushButton_3_clicked()
{
    emit set_floor(3);
}

void MainWindow::on_pushButton_4_clicked()
{
    emit set_floor(4);
}

void MainWindow::on_pushButton_5_clicked()
{
    emit set_floor(5);
}
