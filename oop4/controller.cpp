#include "controller.h"

Controller::Controller(QObject *parent) : QObject(parent), elevator()
{
    state = Controller::FREE;

    QObject::connect(this, SIGNAL(set_target(int )), &elevator, SLOT(call(int )));
    QObject::connect(this, SIGNAL(send_target(int)), this, SLOT(get_floor(int )));
    QObject::connect(&elevator.get_doors(), SIGNAL(doors_closed()), this, SLOT(target_reached()));
}

void Controller::get_floor(int floor)
{
    if (state == Controller::FREE || state == Controller::BUSY)
    {
        if (state == Controller::BUSY)
        {
            std::cout << "Floor " << floor << " added to queue" << "\n";

            queue.push(floor);
            return;
        }

        std::cout << "Lift goes to " << floor << "\n";

        state = Controller::BUSY;
        emit set_target(floor);
        return;
    }
}

void Controller::target_reached()
{
    if (state == Controller::BUSY)
    {
        state = Controller::FREE;

        std::cout << "Target reached" << "\n";

        if (queue.empty())
            return;

        int _floor = queue.front();

        std::cout << "Get " << _floor << " floor from queue" <<"\n";

        queue.pop();

        emit send_target(_floor);
    }
}
