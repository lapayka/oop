#ifndef ELEVATOR_H
#define ELEVATOR_H

#include <QObject>
#include "doors.h"

class elevator : public QObject
{
    Q_OBJECT
public:
    explicit elevator(QObject *parent = nullptr);
    doors &get_doors() {return door; }

    enum
    {
        MOVE_TIME = 500
    };

    enum
    {
        STOP,
        MOVE,
        WAIT
    };

private:
    enum {down = -1, stay = 0, up = 1} direction;
    int cur;
    int target;
    doors door;
    int state;

    QTimer move_timer;

signals:
    void target_reached();
    void start_opening();
    void move_signal();

public slots:
    void stop();
    void move();
    void call(int floor);
};

#endif // ELEVATOR_H
