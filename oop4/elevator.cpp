#include "elevator.h"

elevator::elevator(QObject *parent) : QObject(parent), door()
{
    state = elevator::STOP;

    cur = 1;
    target = 1;

    QObject::connect(this, SIGNAL(target_reached()), this, SLOT(stop()));
    QObject::connect(this, SIGNAL(start_opening()), &door, SLOT(opening()));
    QObject::connect(&move_timer, SIGNAL(timeout()), this, SLOT(move()));
    QObject::connect(this, SIGNAL(move_signal()), this, SLOT(move()));
}

void elevator::stop()
{
    if (state == elevator::MOVE)
    {
        state = elevator::STOP;

        std::cout << "Lift stopped" << "\n";

        emit start_opening();
    }
}

void elevator::move()
{
    int prev_state;
    if ((prev_state = state) == elevator::MOVE || state == elevator::WAIT)
    {
        state = elevator::MOVE;

        if (cur == target)
        {
            emit target_reached();
            return;
        }

        if (prev_state == elevator::MOVE)
        {
            cur += target > cur ? 1 : -1;

            std::cout << "Lift passed floor: " << cur << "\n";
        }
        else if (prev_state == elevator::WAIT)
        {
            std::cout << "Lift goes from " << cur << "\n";
        }

        move_timer.start(elevator::MOVE_TIME);//
    }
}

void elevator::call(int floor)
{
    if (state == elevator::STOP)
    {
        target = floor;

        state = elevator::WAIT;

        emit move_signal();
    }
}
