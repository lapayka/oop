#ifndef DOORS_H
#define DOORS_H

#include <QObject>
#include <QTimer>
#include <iostream>

class doors : public QObject
{
    Q_OBJECT
public:
    explicit doors(QObject *parent = nullptr);

    enum
    {
        OPEN_TIME = 500
    };

    enum
    {
        CLOSED,
        CLOSING,
        OPENING,
        OPENED
    };

private:
    int state;

    QTimer open_timer;
    QTimer close_timer;
    QTimer opened_timer;

signals:
    void doors_closed();
    void start_opening();
    void end_opening();

public slots:
    void opened();
    void closed();
    void opening();
    void closing();
};

#endif // DOORS_H
