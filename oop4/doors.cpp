#include "doors.h"

#define OPEN_TIME doors::OPEN_TIME

doors::doors(QObject *parent) : QObject(parent)
{
    state = CLOSED;

    QObject::connect(&open_timer, SIGNAL(timeout()), this, SLOT(opened()));
    QObject::connect(&opened_timer, SIGNAL(timeout()), this, SLOT(closing()));
    QObject::connect(&close_timer, SIGNAL(timeout()), this, SLOT(closed()));
}

void doors::closed()
{
    if (state == doors::CLOSING)
    {
        state = doors::CLOSED;

        std::cout << "Doors have closed" << "\n";

        emit doors_closed();
    }
}

void doors::closing()
{
    if (state == doors::OPENED)
    {
        state = doors::CLOSING;

        std::cout << "Doors are closing" << "\n";

        close_timer.start(doors::OPEN_TIME);
    }
}

void doors::opened()
{
    if (state == doors::OPENING)
    {
        state = doors::OPENED;

        std::cout << "Doors have opened" << "\n";

        opened_timer.start(doors::OPEN_TIME);
    }
}

void doors::opening()
{
    int wait_time = doors::OPEN_TIME;
    if (state == doors::CLOSING)
    {
        wait_time -= close_timer.remainingTime();
        close_timer.stop();
    }
    if (state == doors::CLOSING || state == doors::CLOSED)
    {
        state = doors::OPENING;

        std::cout << "Doors are opening" << "\n";

        open_timer.start(wait_time);
    }
}
