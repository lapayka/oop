#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <queue>

#include "elevator.h"

class Controller : public QObject
{
private:
    std::queue<int> queue;
    Q_OBJECT

    enum
    {
        BUSY,
        FREE,
    };

private:
    elevator elevator;
    int state;

public:
    explicit Controller(QObject *parent = nullptr);

signals:
    void set_target(int target);
    void send_target(int);

public slots:
    void get_floor(int floor);
    void target_reached();
};

#endif // CONTROLLER_H
