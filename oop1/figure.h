#ifndef FIGURE_H
#define FIGURE_H

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "params.h"
#include "points.h"
#include "edges.h"

typedef struct
{
    points_t points;

    edges_t edges;

    point_t center;
} figure_t;

int read_figure(figure_t &figure, const char *file_name);

int write_figure(const char *file_name, const figure_t &figure);

int transfer_figure(figure_t &figure, const transfer_t *params);

int rotate_figure(figure_t &figure, const rotate_t *rotate);

int scale_figure(figure_t &figure, const scale_t *params);

int destroy_figure(figure_t &figure);


#define READ_ERROR 1
#define WRITE_ERROR 2
#define GET_ERROR 4

#endif // FIGURE_H
