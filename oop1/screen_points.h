#ifndef SCREEN_POINTS_H
#define SCREEN_POINTS_H

#include "point.h"
#include "points.h"
#include "screen_point.h"

typedef struct
{
    screen_point_t *arr;
    int n;
} screen_points_t;

int to_screen_points(screen_points_t &screen_points, points_t &points);

void destroy_screen_points(screen_points_t &points);

#endif // SCREEN_POINTS_H
