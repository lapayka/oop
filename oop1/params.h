#ifndef PARAMS_H
#define PARAMS_H


typedef struct
{
    double x;
    double y;
    double z;
} transfer_t;

typedef struct
{
    double kx;
    double ky;
    double kz;
} scale_t;

typedef struct
{
    double wxy;
    double wxz;
    double wyz;
} rotate_t;

typedef union
{
    scale_t *scale;
    transfer_t *transfer;
    rotate_t *rotate;

    char *file_name;
} params_t;

#endif // PARAMS_H
