#ifndef edge_H
#define edge_H
#include <stdlib.h>
#include <stdio.h>
#include "point.h"

typedef struct
{
    int A;
    int B;
} edge_t;

int is_edge_correct(const edge_t &edge, int n);

int read_edge(edge_t &edge, FILE *file);

void write_edge(FILE *file, const edge_t &edge);

#endif // edge_H
