#include "edges.h"

int is_edges_correct(const edges_t &edges, int n)
{
    int is_correct = edges.arr != NULL;
    int i = 0;
    if (is_correct)
    {
        for (; i < edges.n && is_edge_correct(edges.arr[i], n); i++)
            ;
        is_correct = i == n;
    }

    return is_correct;
}

int read_edges_len(int &n, FILE *file)
{
    int rc = EXIT_SUCCESS;
    if (fscanf(file, "%d", &n) != 1 || n <= 0)
        rc = EXIT_FAILURE;

    return rc;
}

int alloc_edges(edges_t &edges)
{
    int rc = EXIT_SUCCESS;
    edges.arr = (edge_t *)malloc(edges.n * (sizeof(edge_t)));
    if (!edges.arr)
        rc = EXIT_FAILURE;

    return rc;
}

int read_edges(edges_t &edges, FILE *file)
{
    int rc = read_edges_len(edges.n, file);

    if (rc == EXIT_SUCCESS)
    {
        rc = alloc_edges(edges);
        if (rc == EXIT_SUCCESS)
        {
            int i = 0;
            for (; i < edges.n && rc == EXIT_SUCCESS; i++)
                rc = read_edge(edges.arr[i], file);

            if (rc != EXIT_SUCCESS)
                destroy_edges(edges);
        }
    }

    return rc;
}

int write_edges(FILE *file, const edges_t &edges)
{
    for (int i = 0; i < edges.n; i++)
        write_edge(file, edges.arr[i]);

    return EXIT_SUCCESS;
}


void destroy_edges(edges_t *edges)
{
    free(edges->arr);
    edges->arr = NULL;
}
