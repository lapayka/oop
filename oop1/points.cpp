#include "points.h"

int get_points_len(const points_t &points)
{
    return points.n;
}

int is_points_correct(const points_t &points)
{
    return points.arr != NULL && points.n > 0;
}

int read_points_len(int &n, FILE *file)
{
    int rc = EXIT_SUCCESS;
    if (fscanf(file, "%d", &n) != 1 || n <= 0)
        rc = EXIT_FAILURE;

    return rc;
}

int alloc_points(points_t &points)
{
    int rc = EXIT_SUCCESS;
    points.arr = (point_t *)malloc(points.n * (sizeof(point_t)));
    if (!points.arr)
        rc = EXIT_FAILURE;

    return rc;
}

int read_points(points_t &points, FILE *file)
{
    int rc = read_points_len(points.n, file);

    if (rc == EXIT_SUCCESS)
    {
        rc = alloc_points(points);
        if (rc == EXIT_SUCCESS)
        {
            for (int i = 0; i < points.n && rc == EXIT_SUCCESS; i++)
                rc = read_point(points.arr[i], file);

            if (rc != EXIT_SUCCESS)
                destroy_points(points);
        }
    }

    return rc;
}

void write_points_len(FILE *file, const points_t &points)
{
    fprintf(file, "%d\n", points.n);
}


int write_points(FILE *file, const points_t &points)
{
    write_points_len(file, points);

    for(int i = 0; i < points.n; i++)
        write_point(file, points.arr[i]);

    fprintf(file, "\n");

    return EXIT_SUCCESS;
}

int transfer_points(points_t &points, const transfer_t *params)
{
    for (int i = 0; i < points.n; i++)
        transfer_point(points.arr[i], params);

    return EXIT_SUCCESS;
}

int rotate_points(points_t &points, const rotate_t *rotate, const point_t &center)
{
    for (int i = 0; i < points.n; i++)
        rotate_point(points.arr[i], rotate, center);

    return EXIT_SUCCESS;
}

int scale_points(points_t &points, const scale_t *params, const point_t &center)
{
    for (int i = 0; i < points.n; i++)
        scale_point(points.arr[i], params, center);

    return EXIT_SUCCESS;
}

void destroy_points(points_t &points)
{
    free(points.arr);
    points.arr = NULL;
}
