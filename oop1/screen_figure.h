#ifndef SCREEN_FIGURE_H
#define SCREEN_FIGURE_H

#include "screen_points.h"

#include "figure.h"


typedef struct
{
    screen_points_t points;
    const edges_t *edges;
} screen_figure_t;

int to_screen_figure(screen_figure_t &screen_figure, figure_t &figure);

void destroy_screen_figure(screen_figure_t &screen_figure);

#endif // SCREEN_FIGURE_H
