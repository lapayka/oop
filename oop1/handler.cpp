#include "handler.h"

int request_handler(screen_figure_t &screen_figure, request_t request, params_t &params)
{
    int rc = EXIT_SUCCESS;
    static figure_t figure;

    switch (request)
    {
        case TO_SCREEN_FIGURE:
        {
            rc = to_screen_figure(screen_figure, figure);
            break;
        }
        case READ_FIGURE:
        {
            rc = read_figure(figure, params.file_name);
            break;
        }
        case WRITE_FIGURE:
        {
            rc = write_figure(params.file_name, figure);
            break;
        }
        case TRANSFER_FIGURE:
        {
            rc = transfer_figure(figure, params.transfer);
            break;
        }
        case ROTATE_FIGURE:
        {
            rc = rotate_figure(figure, params.rotate);
            break;
        }
        case SCALE_FIGURE:
        {
            rc = scale_figure(figure, params.scale);
            break;
        }
        case DESTROY_FIGURE:
        {
            rc = destroy_figure(figure);
            break;
        }
        default:
            rc = WRONG_COMMAND;
    }

    return rc;
}
