#include "screen_points.h"

int to_screen_points(screen_points_t &screen_points, points_t &points)
{
    screen_point_t *tmp = (screen_point_t *)realloc(screen_points.arr, sizeof(screen_point_t) * points.n);

    int rc = EXIT_SUCCESS;
    if (!tmp)
        rc = EXIT_FAILURE;
    else
    {
        screen_points.arr = tmp;
        screen_points.n = points.n;

        for (int i = 0; i < screen_points.n; i++)
            to_screen_point(screen_points.arr[i], points.arr[i]);
    }

    return rc;
}


void destroy_screen_points(screen_points_t &points)
{
    free(points.arr);
    points.arr = NULL;
}
