#ifndef EDGES_H
#define EDGES_H

#include "edge.h"

typedef struct
{
    edge_t *arr;
    int n;
} edges_t;

int is_edges_correct(const edges_t &edges, int n);

int read_edges(edges_t &edges, FILE *file);

int write_edges(FILE *file, const edges_t &edges);

void destroy_edges(edges_t &edges);

#endif // EDGES_H
