#include "screen_point.h"

void to_screen_point(screen_point_t &screen_point, point_t &point)
{
    screen_point.x = round(point.x - point.y / sqrt(2.0));
    screen_point.y = round(point.z - point.y / sqrt(2.0));
}
