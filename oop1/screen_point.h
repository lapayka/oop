#ifndef SCREEN_POINT_H
#define SCREEN_POINT_H

#include "point.h"

typedef struct
{
    int x;
    int y;
} screen_point_t;

void to_screen_point(screen_point_t &screen_point, point_t &point);

#endif // SCREEN_POINT_H
