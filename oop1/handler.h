#ifndef HANDLER_H
#define HANDLER_H
#include "figure.h"
#include "screen_figure.h"
#include "params.h"

typedef enum
{
    TO_SCREEN_FIGURE,
    READ_FIGURE,
    WRITE_FIGURE,
    TRANSFER_FIGURE,
    ROTATE_FIGURE,
    SCALE_FIGURE,
    DESTROY_FIGURE
} request_t;

#define WRONG_COMMAND 2

int request_handler(request_t request, params_t &params);

#endif // HANDLER_H
