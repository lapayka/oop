#ifndef POINTS_H
#define POINTS_H

#include "point.h"
#include "params.h"

typedef struct
{
    point_t *arr;
    int n;
} points_t;

int get_points_len(const points_t &points);

int is_points_correct(const points_t &points);

int read_points(points_t &points, FILE *file);

int write_points(FILE *file, const points_t &points);

int transfer_points(points_t &points, const transfer_t *params);

int rotate_points(points_t &points, const rotate_t *rotate, const point_t &center);

int scale_points(points_t &points, const scale_t *params, const point_t &center);

void destroy_points(points_t &points);

#endif // POINTS_H
