#include "point.h"

int read_point(point_t &point, FILE *file)
{
    int rc = EXIT_SUCCESS;

    if (fscanf(file, "%lf%lf%lf", &point.x, &point.y, &point.z) != 3)
        rc = EXIT_FAILURE;

    return rc;
}

void write_point(FILE *file, const point_t &point)
{
    fprintf(file, "(%.6lf, %.6lf, %.6lf) ", point.x, point.y, point.z);
}

void transfer_point(point_t &point, const transfer_t *transfer)
{
    point.x += transfer->x;
    point.y += transfer->y;
    point.z += transfer->z;
}

static inline double toRads(double number)
{
    return number * M_PI / 180.0;
}

int rotate_xy(point_t &point, double angle)
{
    double deg_angle = toRads(angle);

    double x = point.x;
    double y = point.y;

    point.x = x * cos(deg_angle) - y * sin(deg_angle);
    point.y = x * sin(deg_angle) + y * cos(deg_angle);

    return EXIT_SUCCESS;
}

int rotate_xz(point_t &point, double angle)
{
    double deg_angle = toRads(angle);

    double x = point.x;
    double z = point.y;

    point.x = x * cos(deg_angle) - z * sin(deg_angle);
    point.z = x * sin(deg_angle) + z * cos(deg_angle);

    return EXIT_SUCCESS;
}


int rotate_yz(point_t &point, double angle)
{
    double deg_angle = toRads(angle);

    double y = point.x;
    double z = point.y;

    point.y = y * cos(deg_angle) - z * sin(deg_angle);
    point.z = y * sin(deg_angle) + z * cos(deg_angle);

    return EXIT_SUCCESS;
}

void rotate_point(point_t &point, const rotate_t *rotate, const point_t &center)
{
    point.x -= center.x;
    point.y -= center.y;
    point.z -= center.z;

    rotate_xy(point, rotate->wxy);
    rotate_xz(point, rotate->wxz);
    rotate_yz(point, rotate->wyz);

    point.x += center.x;
    point.y += center.y;
    point.z += center.z;
}

void scale_point(point_t &point, const scale_t *scale, const point_t &center)
{
    point.x = scale->kx * point.x + center.x * (1.0 - scale->kx);
    point.y = scale->ky * point.y + center.y * (1.0 - scale->ky);
    point.y = scale->kz * point.z + center.z * (1.0 - scale->kz);
}
