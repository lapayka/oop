#ifndef POINT_H
#define POINT_H
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "params.h"

typedef struct
{
    double x;
    double y;
    double z;
} point_t;

int read_point(point_t &point, FILE *file);
void write_point(FILE *file, const point_t &point);
void transfer_point(point_t &point, const transfer_t *transfer);
void rotate_point_z(point_t &point, double angle);
void rotate_point_y(point_t &point, double angle);
void rotate_point_x(point_t &point, double angle);
void scale_point(point_t &point, const scale_t *scale, const point_t &center);
void rotate_point(point_t &point, const rotate_t *rotate, const point_t &center);



#endif // POINT_H
