#include "screen_figure.h"

int to_screen_figure(screen_figure_t &screen_figure, figure_t &figure)
{
    int rc = to_screen_points(screen_figure.points, figure.points);

    if (rc == EXIT_SUCCESS)
        screen_figure.edges = &figure.edges;

    return rc;
}

void destroy_screen_figure(screen_figure_t &screen_figure)
{
    destroy_screen_points(screen_figure.points);
}
