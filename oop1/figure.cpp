#include "figure.h"

void figure_init(figure_t *figure)
{
    memset(figure, 0, sizeof(figure_t));
}

int is_figure_correct(const figure_t &figure)
{
    return is_points_correct(figure.points) && is_edges_correct(figure.edges, get_points_len(figure.points));
}

int read_figure(figure_t &figure, const char *file_name)
{
    FILE *file = fopen(file_name, "r");
    int rc = EXIT_SUCCESS;
    figure_t copy;
    if (!file)
        rc = EXIT_FAILURE;
    else
    {
        figure_init(&copy);
        if ((rc = read_points(copy.points, file)) == EXIT_SUCCESS)
        {
            if ((rc = read_edges(copy.edges, file)) != EXIT_SUCCESS)
                destroy_points(figure.points);
            else if ((rc = read_point(copy.center, file)) != EXIT_SUCCESS)
                destroy_figure(copy);
        }
    }

    fclose(file);

    if (rc == EXIT_SUCCESS)
    {
        if (!is_figure_correct(copy))
            rc = EXIT_FAILURE;
        else
        {
            destroy_figure(figure);
            figure = copy;
        }
    }

    return rc;
}

int write_figure(const char *file_name, const figure_t &figure)
{
    if (!file_name || !is_figure_correct(figure))
        return EXIT_FAILURE;
    FILE *file;

    file = fopen(file_name, "w");
    int rc;

    if (!file)
        rc = EXIT_FAILURE;
    else
    {
        rc = write_points(file, figure.points);
        if (rc == EXIT_SUCCESS)
        {
            rc = write_edges(file, figure.edges);
            if (rc == EXIT_SUCCESS)
                write_point(file, figure.center);
        }
    }

    fclose(file);
    return rc;
}

int transfer_figure(figure_t &figure, const transfer_t *params)
{
    if (!is_figure_correct(figure))
        return EXIT_FAILURE;
    transfer_points(figure.points, params);
    return EXIT_SUCCESS;
}

int rotate_figure(figure_t &figure, const rotate_t *rotate)
{
    if (!is_figure_correct(figure))
        return EXIT_FAILURE;
    rotate_points(figure.points, rotate, figure.center);
    return EXIT_SUCCESS;
}

int scale_figure(figure_t &figure, const scale_t *params)
{
    if (!is_figure_correct(figure))
        return EXIT_FAILURE;
    scale_points(figure.points, params, figure.center);
    return EXIT_SUCCESS;
}

int destroy_figure(figure_t &figure)
{

    destroy_points(figure.points);
    destroy_edges(figure.edges);
    return EXIT_SUCCESS;
}
