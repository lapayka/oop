#include "edge.h"

int is_edge_correct(edge_t &edge, int n)
{
    return edge.A < n && edge.B < n;
}

int read_edge(edge_t &edge, FILE *file)
{
    int rc = EXIT_SUCCESS;
    if (fscanf(file, "%d%d", &edge.A, &edge.B) != 2)
        rc = EXIT_FAILURE;

    return rc;
}

void write_edge(FILE *file, edge_t &edge)
{
    fprintf(file, "{%d,%d} ", edge.A, edge.B);
}
